# DATABÁZE OBCÍ

## VOSPlzeň, Základy programování, 2. semestr
---
* konzolová aplikace imitující prostředí MS-DOS za využití OOP
* program načítá obce z databáze v CSV
* pomocí uvedených kláves lze listovat dopředu a dozadu, příp. o deset stran tam nebo zpět
* obce lze filtrovat např. podle obyvatel nebo kraje